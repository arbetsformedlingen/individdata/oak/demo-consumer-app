import { SolidNodeClient } from 'solid-node-client';
import { baseUrl, sourceUrl} from './constants.js';
import { readFileSync } from 'fs';
import * as vc from './vc.js';
import * as solid from './solid.js';
//import * as zlib from 'zlib';
import express from "express";

//const pnr = "870820-5441";

const context = {
    id: "@id",
    type: "@type",
    status: "schema:status",
    subject: "http://purl.org/dc/terms/subject",
    name: "schema:name",
    familyName: "schema:familyName",
    givenName: "schema:givenName",
    identifier: "schema:idenifier",
    statusChangedDate: "schema:statusChangedDate",
    issued: "schema:issued",
    issuer: "schema:creator",
    Person: "schema:Person",
    GovernmentOrganization: "schema:GovernmentOrganization",
    UnemploymentStatus: "schema:DigitalDocument",
    schema: "http://schema.org/"
};

const credentialSubject = (pnr) => { return {
    type: "UnemploymentStatus",
    status: "unemployed",
    subject: `"${pnr}"`,
    statusChangedDate: "2021-09-17",
    issuer: {
      type: "GovernmentOrganization",
      identifier: "202100-2114",
      name: "Arbetsförmedlingen"
    }
}};

async function createVc(id, responseUrl, pnr) {
    // Sign and upload document
    const credential = vc.createCredential(context, credentialSubject(pnr));
    //console.dir(credential, { depth: null });

    const signedJSON = await vc.issue(key, credential);
    const signed = JSON.stringify(signedJSON, null, 0);
    //console.log("len signed: ", signed.length);

    //const deflatedVc = zlib.deflateSync(signed).toString('base64');
    //const deflatedVc = deflate(signed).toString('base64');
    //const deflatedVc = signed.toString('base64');
    //console.log("deflatedVc: ", deflatedVc);
    //console.log("len deflatedVc: ", deflatedVc.length);

    const jsonld = {
        "@context": [
            {
                "schema": "http://schema.org/"
            }
        ],
        "@type": "schema:Document"
    };

    jsonld["@id"] = responseUrl;
    jsonld["urn:id"] = id;
    jsonld["schema:text"] = signed;

    return jsonld;
}

async function handleDataRequest(requestLinkUrl) {
    const requestLink = await solid.getJsonld(session, requestLinkUrl);
    //console.dir(requestLink, { depth: null });
    
    const requestUrl = requestLink[0]['https://oak.se/dataRequest'].map((m) => m['@id'])[0]
    //console.log(requestUrl)
    
    const request = await solid.retry(() => solid.getJsonld(session, requestUrl));
    //console.dir(request, { depth: null });
     
    const id = request[0]['http://schema.org/identifier'].map((m) => m['@value'])[0]
    const responseUrl = request[0]['https://oak.se/dataResponseUrl'].map((m) => m['@id'])[0]
    //console.log(responseUrl)
    
    const pnr = request[0]['https://oak.se/subjectId'].map((m) => m['@value'])[0]
    //console.log(pnr)
    
    const jsonld = await createVc(id, responseUrl, pnr);
    //console.dir(jsonld, { depth: null });
    await solid.retry(() => solid.putJsonld(session, responseUrl, jsonld));

    const dataResponseLinkPath = (agent, resource) => `${baseUrl}/${agent}/oak/inbox/response-link-${resource}`;
    const dataResponseLink = ((url) => `<> <https://oak.se/dataResponseUrl> <${url}>.`);
    await solid.putFile(session, dataResponseLinkPath("user", id), dataResponseLink(responseUrl), 'text/turtle');
    await solid.deleteFile(session, requestLinkUrl);
}

// Log in to pod
const credentials = JSON.parse(readFileSync('./source-credentials.json'));
const client = new SolidNodeClient();
const session = await client.login(credentials);

if(!session.isLoggedIn) throw new Error("Could not login as source");
console.log("Logged in as: ", session.webId);

// Write the key
const key = await vc.loadKey("source-key.pem", {id: `${sourceUrl}/key`, controller: `${sourceUrl}/controller`});

//console.log("key:", JSON.stringify(vc.publicKeyDoc(key), null, 2));
//console.log("controller:", JSON.stringify(vc.controllerDoc(key), null, 2));

await solid.putPublicJsonld(session, key.id, vc.publicKeyDoc(key));
await solid.putPublicJsonld(session, key.controller, vc.controllerDoc(key));

const inbox = `${baseUrl}/source/oak/inbox/`;

// read inbox folder
const document = await solid.getJsonld(session, inbox);
console.dir(document, { depth: null });
//let contains = null;
/* for (let i in document) {
    const doc = document[i];
    contains = doc['http://www.w3.org/ns/ldp#contains']
    if (contains) {
        break;
    }
}*/
const contains = document[0]['http://www.w3.org/ns/ldp#contains'];
if (contains) {
    const requests = contains.map((m) => m['@id'])
    //console.dir(requests, { depth: null });
    
    for (const nr in requests) {
        //console.dir(requests[nr], { depth: null });
        const requestLinkUrl = requests[nr];
        //console.log("requestLinkUrl: " + requestLinkUrl);
        await handleDataRequest(requestLinkUrl);
    }
} else {
    console.log("nothing to process ...");
}

console.log("starting webhook handling ...");
const subscrdata = {
    "@context": ["https://www.w3.org/ns/solid/notification/v1"],
    type: "WebHookSubscription2021",
    topic: `${baseUrl}/source/oak/inbox/`,
    target: "http://localhost:9999/webhook",
};

function isCreate(notfication) {
    const type = notfication["type"][0];
    return "Create" === type;
}

function getResourse(notfication) {
    const object = notfication["object"];
    return object["id"];
}

const app = express();
app.use(express.json());
app.post("/webhook", (req, res) => {
    const notification = req.body;
//    console.log("notfication: ", notification);
//    console.log("isCreate: ", isCreate(notification));
    if (isCreate(notification)) {
        const requestLinkUrl = getResourse(notification);
        handleDataRequest(requestLinkUrl)
    }
    res.sendStatus(200);
});

app.listen(9999);

const subscription = `${baseUrl}/subscription`; // should be taken from well-known ..
await solid.postFile(
    session,
    subscription,
    JSON.stringify(subscrdata),
    "application/json"
);
console.log("webhook subscription started.");

/// Logout
//await session.logout();
// even after logout the session still has timers that stop node from exiting.
//process.exit(0);
