import fetch from 'node-fetch';

function getBaseUrl() {
    const n = process.argv.length;
    if (n === 3) {
      return process.argv[2];
    } else {
      return "http://localhost:3000";
    }
  }

const baseUrl = getBaseUrl();
const registerUrl0 = baseUrl + "/idp/register"; // Ending slash must NOT be present
const registerUrl = baseUrl + "/idp/register/"; // Ending slash mandatory
const setupUrl = baseUrl + "/setup"; // Ending slash must NOT be present

function createParams0(name) {
  return new URLSearchParams({
    createWebId: "on",
    webId: "",
    register: "on",
    createPod: "on",
    podName: name,
    email: name + "@example.com",
    password: name,
    confirmPassword: name,
    submit: ""
  });
}

function createParams(name) {
  return new URLSearchParams({
    createWebId: "on",
    register: "on",
    createPod: "on",
    name: name,
    podName: name,
    email: name + "@example.com",
    password: name,
    confirmPassword: name
  });
}

async function register(name) {
  const params = createParams(name);
  const response = await fetch(
    registerUrl,
    {method: "POST",
    body: params,
    headers: { "Content-type": "application/x-www-form-urlencoded" }
  });
  console.log("response: " + await response.text());
  return response;
}

console.log(await fetch(setupUrl, {method: 'POST'}));

console.log((await register("sink")));
console.log((await register("source")));
console.log((await register("user")));
