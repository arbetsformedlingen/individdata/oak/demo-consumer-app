import { WebSocketServer } from 'ws';
import * as http from 'http';
import { SolidNodeClient } from 'solid-node-client';
import { baseUrl } from './constants.js';
import { readFileSync } from 'fs';
import * as vc from './vc.js';
import * as solid from './solid.js';
import * as zlib from 'zlib';
import jsonld from 'jsonld';
import express from "express";
import expressLayouts from 'express-ejs-layouts';
import ejs from 'ejs';
import { fileURLToPath } from 'url';
import path from 'path';
import { dirname } from 'path';
import { v4 as uuidv4 } from 'uuid';

const port = 9998;

const dataRequest = ((id, pnr) => `
<> a <http://oak.se/UnemploymentCertificateDataRequest>;
    <http://schema.org/identifier> "${id}";
    <http://purl.org/dc/elements/1.1/subject> <${baseUrl}/user/profile/card#me>;
    <https://oak.se/subjectId> "${pnr}";
    <https://oak.se/purpose> "Ansökan om arbetslöshetsförsäkring";
    <https://oak.se/requestedData> "datum", "anställningsstatus", "personnummer";
    <http://schema.org/sourceOrganization> <${baseUrl}/source/profile/card#me>;
    <http://www.w3.org/2007/ont/link#requestedBy> <${baseUrl}/sink/profile/card#me>.
`);

const dataRequestAcl = ((id) => `
<#user> a <http://www.w3.org/ns/auth/acl#Authorization>;
    <http://www.w3.org/ns/auth/acl#accessTo> <${baseUrl}/sink/oak/requests/request-${id}>;
    <http://www.w3.org/ns/auth/acl#agent> <mailto:user@example.com>, <${baseUrl}/user/profile/card#me>;
    <http://www.w3.org/ns/auth/acl#mode> <http://www.w3.org/ns/auth/acl#Read>.
<#owner> a <http://www.w3.org/ns/auth/acl#Authorization>;
    <http://www.w3.org/ns/auth/acl#accessTo> <${baseUrl}/sink/oak/requests/request-${id}>;
    <http://www.w3.org/ns/auth/acl#agent> <mailto:sink@example.com>, <${baseUrl}/sink/profile/card#me>;
    <http://www.w3.org/ns/auth/acl#mode> <http://www.w3.org/ns/auth/acl#Write>, <http://www.w3.org/ns/auth/acl#Read>, <http://www.w3.org/ns/auth/acl#Control>.
`);

const dataRequestLink = ((id) => `
<> <https://oak.se/dataRequest> <${baseUrl}/sink/oak/requests/request-${id}>.
`);

const dataRequestPath = (agent, id) => `${baseUrl}/${agent}/oak/requests/request-${id}`;
const dataRequestAclPath = (agent, id) => dataRequestPath(agent, id) + '.acl';
const dataRequestLinkPath = (agent, id) => `${baseUrl}/${agent}/oak/inbox/request-link-${id}`;

async function createRequest() {
  const id = uuidv4();
  const p1 = solid.putFile(session, dataRequestPath("sink", id), dataRequest(id, "870820-5441"), 'text/turtle');
  const p2 = solid.putFile(session, dataRequestAclPath("sink", id), dataRequestAcl(id), 'text/turtle');
  const p3 = solid.putFile(session, dataRequestLinkPath("user", id), dataRequestLink(id), 'text/turtle');
  await Promise.all([p1, p2, p3]);
}

async function addRequests(n) {
  for (let i = 0; i < n; ++i) {
    await createRequest();
  }
}

async function handleDataResponse(responseLinkUrl){
  const responseLink = await solid.getJsonld(session, responseLinkUrl);
  // console.dir(responseLink, { depth: null });
      
  const responseUrl = responseLink[0]['https://oak.se/dataResponseUrl'].map((m) => m['@id'])[0]
  console.log(responseUrl)
      
  const response = await solid.getJsonld(session, responseUrl);
  // console.dir(response, { depth: null });
  
  const compacted = await jsonld.compact(response, {});
  // console.log("Compacted:");
  // console.dir(compacted, { depth: null });
  const signed = compacted["http://schema.org/text"];
  //const deflatedVc = compacted["http://schema.org/text"];
  //const inflatedVc = zlib.inflateSync(new Buffer.from(deflatedVc, 'base64')).toString();
  const vcJSON = JSON.parse(signed);
  //    console.log("Verifiable credential:");
  //    console.dir(vcJSON, { depth: null });
      
  const docLoader = vc.documentLoaderGenerator(session);
  const verification = await vc.verify(vcJSON, docLoader);
      // TODO: Here we could verify the public key with a CA
  //    console.log("Verification:");
  //    console.dir(verification, { depth: null });
  const isVerified = verification["verified"];

  if (isVerified) {
    const credentialSubject = vcJSON["credentialSubject"];
    console.log("Verified DataResponse: [" + responseUrl) + "]";
    console.dir(credentialSubject, { depth: null });
    return credentialSubject;
  } else {
    console.log("Could not verify DataResponse: [" + responseUrl) + "]";
  }
  await solid.deleteFile(session, responseLinkUrl);
}

// Log in to pod
const credentials = JSON.parse(readFileSync('./sink-credentials.json'));
const client = new SolidNodeClient();
let session = await client.login(credentials);

if(!session.isLoggedIn) throw new Error("Could not login as source");
console.log("Logged in as: ", session.webId);



const inbox = `${baseUrl}/sink/oak/inbox/`;

// read inbox directory
const doc = await solid.getJsonld(session, inbox);
//console.dir(doc, { depth: null });
const contains = doc[0]['http://www.w3.org/ns/ldp#contains'];
if (contains) {
  const responses = doc[0]['http://www.w3.org/ns/ldp#contains'].map((m) => m['@id'])
//  console.dir(responses, { depth: null });
  
  for (const nr in responses) {
//    console.dir(responses[nr], { depth: null });
    const responseLinkUrl = responses[nr];
//    console.log("responseLinkUrl: " + responseLinkUrl);
    await handleDataResponse(responseLinkUrl);
  }

} else {
  console.log("nothing to process ...");
}

console.log("starting webhook handling ...");
const subscrdata = {
    "@context": ["https://www.w3.org/ns/solid/notification/v1"],
    type: "WebHookSubscription2021",
    topic: `${baseUrl}/sink/oak/inbox/`,
    target: "http://localhost:9998/webhook",
};

function isCreate(notfication) {
    const type = notfication["type"][0];
    return "Create" === type;
}

function getResourse(notfication) {
    const object = notfication["object"];
    return object["id"];
}

const app = express();
const server = http.createServer(app);
// websocket
const wss = new WebSocketServer({ server: server});
var gws;
wss.on('connection', function connection(ws) {
  console.log("New client connected!");
  gws = ws;
});

let notfications = [];
app.use(express.json());
app.post("/webhook", (req, res) => {
    const notification = req.body;
    notfications.push(JSON.stringify(notification));
    //console.log("notfication: ", notification);
    //console.log("isCreate: ", isCreate(notification));
    if (isCreate(notification)) {
        const responseLinkUrl = getResourse(notification);
        let verifiedData = handleDataResponse(responseLinkUrl);
        verifiedData.then(function(result) {
          console.log("verified data: ", result);
          gws.send(JSON.stringify(result));
        });  
    }
    res.sendStatus(200);
});

app.post("/action", (req, res) => {
  const {method, number} = req.body;
  console.log("method: ", method);
  console.log("number: ", number);
  if (method === "addRequests") {
    const p = addRequests(number);
    p.then(() => {
      console.log(number + "request(s) added.");
    }).catch((err) => {
      console.log(err);
    })
  }
  res.sendStatus(200);
});


// Static Files
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
app.use(express.static(path.join(__dirname, '/public')))


// Set templating Engine
app.use(expressLayouts);
app.set('layout', './layouts/full-width');
app.set('view engine', 'ejs');

// Navigation
app.get("/", (req, res) => {
  res.render(`index`, { title: 'Demo Consumer App', text: notfications});
});

server.listen(port);
console.log(`Server started at: http://localhost:${port}`);

const subscription = `${baseUrl}/subscription`; // should be taken from well-known ..
await solid.postFile(
    session,
    subscription,
    JSON.stringify(subscrdata),
    "application/json"
);
console.log("webhook subscription started.");


// Logout
//await session.logout();
// even after logout the session still has timers that stop node from exiting.
//process.exit(0);
